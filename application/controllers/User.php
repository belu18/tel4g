<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('assets');
        $this->load->helper('form');
    }
    public function index()
    {
    	$this->load->view('theme/header');
        if (null!==($this->session->userdata('prenom'))) {
            redirect('abo','refresh');
            // si on a une session avec prenom d'ouverte 
            // il redirige automatiquement vers nos offres
        }
        else
        {
            // les différentes règle d'accpetance paour chaque champs
            $this->form_validation->set_rules('nom','Nom','trim|required|alpha_dash|encode_php_tags',
            									array(	'required'=>'%s est requi0. s'));
        	$this->form_validation->set_rules('prenom','Prenom','trim|required|alpha_dash|encode_php_tags|min_length[5]|max_length[20]', 
            									array(	'required'=>'%s. est requis. ',
            											'min_length'=>'%s doit contenir minimum 5 caractères. ',
            											'max_length'=>'%s doit contenir maximum 20 caractères. ',
            											));
            
            $this->form_validation->set_rules('email','Email','trim|required|encode_php_tags|valid_email|is_unique[users.email]',
            									array(	'required'=>'%s est requis. ',
            											'valid_email'=>'%s doit être une adresse valide. ',
            											'is_unique'=>'Cet %s est déjà utilisé .'
            										));
            $this->form_validation->set_rules('datenaiss','Date de naissance');

            $this->form_validation->set_rules('mdp1','Mot de passe','trim|required|alpha_dash|encode_php_tags|min_length[5]',
            									array(	'required'=>'%s est requis. ',
            											'alpha_dash'=>'%s doit contenir des chiffres ou des lettres ou des underscores. ',
            											'min_length'=>'%s doit avoir minimum 5 caractères. '
            										));
            $this->form_validation->set_rules('mdp2','Confirmation du mot de passe','trim|required|min_length[5]|matches[mdp1]',								array(	'required'=>'%s est requis. ',
            											 'matches'=>'La confirmation du mot de passe doit être égale au mot de passe. '
            										));
        	if ($this->form_validation->run() == FALSE)
            {
            	$this->load->view('user/body_form');// en cas de non validation du formulaire on renvoi le formulaire
            }
            else
            {
        		$this->validation();
            }
        }
    }
    public function validation()
    { 
       	$nom=$this->input->post('nom');       
        $prenom=$this->input->post('prenom');       
        $email=$this->input->post('email');       
        $datenaiss=$this->input->post('datenaiss');       
        $mdp1=$this->input->post('mdp1');       
        $mdp2=$this->input->post('mdp2');      
   		$this->load->model('users_model');
   		$result= $this->users_model->insert_users($nom,$prenom,$email,$datenaiss,$mdp1);
   		redirect('abo');
    }
}