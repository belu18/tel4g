<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Authentification extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('assets');
    }
    public function index()
    {
        $this->load->view('theme/header');
        //$this->load->view('user/connexion');
        if (null!==($this->session->userdata('prenom'))) {
            redirect('abo','refresh');
            // si on a une session avec prenom d'ouverte 
            // il redirige automatiquement vers nos offres
        }
        else
        {
            $this->load->model('users_model');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $result= $this->users_model->login_user($email,$password);

            
            $this->form_validation->set_rules('email', '"Email"', 'trim|required|valid_email',
                                            array(  'required'=>'%s est requis .',
                                                    'valid_email'=>'%s n\' est pas une adresse valide .' ));
            $this->form_validation->set_rules('password', '"Mot de passe"', 'trim|required',
                                            array('required'=>'%s est requis .'));
            if ($this->form_validation->run() == FALSE)
            {
                $this->load->view('user/connexion');
            }
            elseif ($this->form_validation->run() == true && empty($result)) 
            {
                
                $this->erreur_connexion();
            }
            else
            {
               // var_dump($result[0]);
                $this->session->set_userdata('id_user', $result[0]->id_user);
                $this->session->set_userdata('prenom', $result[0]->prenom);
                redirect('phones');
            }
        }
        
        /*$data=array();
        $data['phone_info'] = $this->phones_model->get_info(); 
    	$this->load->view('theme/header');
        $this->load->view('phones/body',$data);*/
    }
    public function logout()
    {
        $this->session->sess_destroy();
        redirect('phones');        
    }
    public function erreur_connexion()
    {
        //on charge modèle qui verifie si email existe
        $this->load->model('users_model');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $result_email= $this->users_model->erreur_email($email);
        //ternaire si email vide alors email inconue sinon mot de passe invalide
        // on verifie vide car si modèle ne retourne rien de la bdd il retourne un empty
         $erreur=array();
        empty($result_email) ? $erreur['error']='E-mail inconnue' : $erreur['error']="Mot de passe invalide" ;
        $this->load->view('user/connexion',$erreur);
    }
}