<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Phones extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('assets');
    }
    public function index()
    {
        $this->load->model('phones_model');
        $data=array();
        $data['phone_info'] = $this->phones_model->get_info(); 
    	$this->load->view('theme/header');
        $this->load->view('phones/body',$data);
    }
    public function detail($id)
    {
        $this->load->model('phones_model');
        $data=array();
        $data['phone_info'] = $this->phones_model->get_detail($id); 
        $this->load->view('theme/header');
        $this->load->view('phones/detail',$data);
        
    }
}