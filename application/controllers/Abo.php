<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Abo extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('assets');
    }
    
    public function index()
    {
        $this->load->model('abos_model');
        $data=array();
        $data['abos_info'] = $this->abos_model->get_info_abos(); 
        $data['options_info'] = $this->abos_model->get_info_options(); 
        $this->load->view('theme/header');
      /*  $data["abonnement1"]=array(
                                                        "nom"=>"Bas prix",
                                                        "prix"=>2.49,
                                                        "communications"=>"2h de communications",
                                                        "destination"=>"Vers fixe et mobiles",
                                                        "zone"=>"Nationale"
                                                         );
         $data["abonnement2"]=array(                   "nom"=>"National",
                                                        "prix"=>9.99,
                                                        "communications"=>"Téléphonie et SMS illimmités",
                                                        "destination"=>"Vers fixe et mobiles",
                                                        "zone"=>"Nationale"
                                                         );
         $data["abonnement3"]=array(
                                                        "nom"=>"EURO",
                                                        "prix"=>19.99,
                                                        "communications"=>"Téléphonie et SMS illimmités",
                                                        "destination"=>"Vers fixe et mobiles",
                                                        "zone"=>"Europe"
                                                         );
        $data["option1"]=array(
                                                        "nom"=>"Data",
                                                        "prix"=>5,
                                                        "giga"=>3
                                                        );
        $data["option2"]=array(
                                                        "nom"=>"Data MAX",
                                                        "prix"=>10,
                                                        "giga"=>20
                                                        );*/
        /*$this->load->view('theme/header');*/
        $this->load->view('abos/body_abos', $data);
    }
    
}

