	<title>Rejoignez-nous</title>
</head>
<body>
	<div class="row justify-content-center">
		<h1 class="col-4">Inscription</h1>
	</div>
	<div class="row">
		<div class="col-md-10 offset-2" id="erreur_inscription" style="color: red;">
		<?= validation_errors(); ?>
		</div>
	</div>
	<div class="row">
		<?php echo form_open('user', ' class="col-md-8 offset-2" id="user"'); ?>
		<!-- <form class="col-md-8 offset-2" method="post" id="user" action="user"> -->
			<div class="form-group row">
		 		<label for="exam-ple-text-input" class="col-sm-2 col-form-label">Nom *</label>
			 	<div class="col-md-6">
			    	<input class="form-control" type="text" placeholder="Votre nom" name="nom"/>
			    </div>
			</div>
			
			<div class="form-group row">
		 		 <label for="example-text-input" class="col-sm-2 col-form-label">Prénom *</label>
			 	<div class="col-md-6">
			    	<input class="form-control" type="text"  placeholder="Votre prenom" name="prenom"/>
			    </div>
			</div>
			<div class="form-group row">
		 		 <label for="example-text-input" class="col-sm-2 col-form-label">Date naissance</label>
			 	<div class="col-md-6">
			    	<input class="form-control" type="date" name="datenaiss"/>
			    </div>
			</div><div class="form-group row">
		 		 <label for="example-text-input" class="col-sm-2 col-form-label">Adresse e-mail *</label>
			 	<div class="col-md-6">
			    	<input class="form-control" type="email" name="email" placeholder="exemple@exemple.com"/>
			    </div>
			</div><div class="form-group row">
		 		 <label for="example-text-input" class="col-sm-2 col-form-label">Mot de passe *</label>
			 	<div class="col-md-6">
			    	<input class="form-control" type="password" name="mdp1" placeholder="Votre mot de passe"/>
			    </div>
			</div><div class="form-group row">
		 		 <label for="example-text-input" class="col-sm-2 col-form-label">Confirmer mot de passe *</label>
			 	<div class="col-md-6">
			    	<input class="form-control" type="password" name="mdp2" placeholder="Confirmation de votre mot de passe"/>
			    </div>
			</div>		  
			 <button type="submit" class="btn btn-primary" id="bsubmit">Envoyer</button>
		<!-- </form> -->
		<?= form_close(); ?>
	</div>
	
</body>