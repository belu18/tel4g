	<title>Identifiez-vous</title>
</head>
<body>
	<div class="row justify-content-center">
		<h1 class="col-4">Identifiez-vous</h1>
	</div>
	<div class="row">
		<div class="col-md-10 offset-2" id="erreur_connexion" style="color: red;">
		<?= validation_errors(); ?>
		<?php if (isset($error)) {
			echo $error;
		} ?>
		</div>
	</div>
	<div class="row">
		<?php echo form_open('authentification', ' class="col-md-8 offset-2" id="connexion"'); ?>
		<!-- <form class="col-md-8 offset-2" method="post" id="user" action="user"> -->
			<div class="form-group row">
		 		<label for="exam-ple-text-input" class="col-sm-2 col-form-label">E-mail *</label>
			 	<div class="col-md-6">
			    	<input class="form-control" type="email" placeholder="Votre adresse mail de connexion" name="email"/>
			    </div>
			</div>
			<div class="form-group row">
		 		<label for="exam-ple-text-input" class="col-sm-2 col-form-label">Mot de passe *</label>
			 	<div class="col-md-6">
			    	<input class="form-control" type="password" placeholder="Votre mot de passe" name="password"/>
			    </div>
			</div>
			<button type="submit" class="btn btn-primary" id="bsubmit">Me connecter</button>
			<a class="btn btn-info offset-3" href="<?php echo base_url('index.php/user');?>">Je souhaite m'inscrire</a>
			
		<?= form_close(); ?>
		</div>
			
	</div>