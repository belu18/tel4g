<!DOCTYPE HTML>
<html>
<head>
 	<meta charset="utf-8"> 
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
 	<link href="<?php echo css_url('style');?>" rel="stylesheet" type="text/css">
 	<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
 	<script src="<?= js_url('script');?>"></script>
 	 <style type="text/css">
      [class*="col"] { margin-bottom: 20px; }
      img { width: 100%; }
    </style>
  	<nav class="navbar navbar-inverse bg-primary navbar-toggleable-md bg-faded">
	    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
 			 <span class="navbar-toggler-icon"></span>
  		</button>
  		<a class="navbar-brand" href="<?php echo base_url('abo');?>">Tel4G</a>
 		 <div class="collapse navbar-collapse" id="nav-gauche">
  		 	<div class="navbar-nav">
	    		<a class="nav-item nav-link" href="<?php echo base_url('abo');?>">Abonnements </a>
	     		<a class="nav-item nav-link" href="<?php echo base_url('phones');?>">Téléphones</a>
    		</div>
    		<div class="navbar-nav offset-md-7" id="nav-droite">
          <?php if ($this->session->userdata('prenom'))
          { // on affiche le prenom de la personne si elle est connecté en session avec un lien deconnexion
              $prenom=$this->session->userdata('prenom');
              ?>
              <li  class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 <?= $prenom ?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="<?php echo base_url('authentification/logout');?>">Deconnexion</a>
                </div>
              </li>
               <?php
              
          }// si pas de session alors on affiche les liens inscriptions et connexion
          else{?>
  	    		<a class="nav-item nav-link" href="<?php echo base_url('user');?>">Inscription</a>
  	     		<a class="nav-item nav-link" href="<?php echo base_url('authentification');?>">Connexion</a><?php
          }
          ?>
    		</div>
  		</div>
	</nav>
    
