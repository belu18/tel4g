	<title>Nos offres</title>
</head>
<body>
	<div class="row">
		<h1 class="col-sm-3 offset-3">Abonnements</h1>
	</div>
	<div class="row">
		<div class="card-deck" id="abos">
		<?php
			foreach ($abos_info as $abos) {?><!--  on récupère les données de la bdd via le select du model et on utilise un foeach pour les parcourir -->

				<div class=" col-md-4 card mb-3 text-center subscription" id="<?= $abos->id_abonnements ?>">
	 				 	<div class="card-block">
		 				 	<h2 class="card-title"><?= $abos->nom ?></h2>
		 				 	<h1 class="card-title"><?= $abos->prix ?> €/mois</h1>
		 				 	<h5 class="card-title"><?= $abos->communication ?></h5>
		 				 	<h5 class="card-title"><?= $abos->destinataire ?></h5>
		 				 	<h5 class="card-title"><?= $abos->zone ?></h5>
	  					</div>
				</div>			
		<?php }?>

		</div>
	</div>
	<div class="row">
		<h1 class="col-sm-3 offset-3">Options</h1>
	</div>
	<div class="row">
		<div class="card-deck" id="options">
		<?php 
			foreach ($options_info as $options) { ?><!--  on récupère les données de la bdd via le select du model et on utilise un foeach pour les parcourir -->
				<div class=" col-md-4 card  mb-3 options text-center" style="width: 20rem;" id="<?= $options->id_options ?>">
	 				 	<div class="card-block" >
		 				 	<h2 class="card-title"><?= $options->nom ?></h2>
		 				 	<h1 class="card-title"><?= $options->prix ?> €/mois</h1>
		 				 	<h5 class="card-title"><?= $options->giga ?> giga</h5>
	  					</div>
				</div>			
		<?php } ?>

		</div>
	</div>
	<div class="row">
		
 		 	<a href="#" class="btn btn-primary btn-default" style="visibility: hidden;" id='Mycollapse'>Valider <span class="glyphicon glyphicon-saved"></span></a>
 		 
 	</div>
</body>
</html>