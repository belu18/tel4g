<body>
	<?php
		foreach ($phone_info as $phone) {?><!--  on récupère les données de la bdd via le select du model et on utilise un foeach pour les parcourir -->
		<div class="row">
			<div class="col-md-2 offset-4 h-50">
				<h2><?= $phone->marque ?></h2>
			</div>
			<div class="col-md-6 h-50">
				<h2><?= $phone->modele ?></h2>
			</div>
		</div>
		<div class="row">
			<div class=" col-sm-3 offset-3 card text-center" id="<?= $phone->id_tel ?>">
  				<img class="card-img-top" src="<?= base_url('assets/images/telephones/'.$phone->image)?>" alt="<?= $phone->image?>" style=" height:300px;">
 				 	<div class="card-block">
	 				 	<h1 class="card-title"><?= $phone->prix ?> €</h1>
  					</div>
			</div>	
			<table class=" col-md-6  table-bordered table-striped table-responsive">
				<thead>
					<tr class="table-info">
						<th>Caractéristique</th>
						<th>Valeur</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Poids(g)</td>
						<td><?= $phone->poids ?></td>
					</tr>
					<tr>
						<td>Hauteur (mm)</td>
						<td><?= $phone->hauteur ?></td>
					</tr>
					<tr>
						<td>Largeur(mm)</td>
						<td><?= $phone->largeur ?></td>
					</tr>
					<tr>
						<td>Epaisseur(mm)</td>
						<td><?= $phone->epaisseur ?></td>
					</tr>
					<tr>
						<td>Autonomie(h)</td>
						<td><?= $phone->autonomie ?></td>
					</tr>
					<tr>
						<td>System d'expoitation</td>
						<td><?= $phone->os ?></td>
					</tr>
					<tr>
						<td>Capacité mémoire (Go)</td>
						<td><?= $phone->capacite_memoire ?></td>
					</tr>
					 <tr>
						<td>Extension memoire</td>
						<td><?php 
							if ($phone->os>0) {
							 	echo '<div class="form-check disabled">
										  <label class="form-check-label">
										    <input class="form-check-input" type="checkbox" value="" checked disabled>
										  </label>
										</div>';
							 } 
							 else{
							 	echo '<div class="form-check disabled">
										  <label class="form-check-label">
										    <input class="form-check-input" type="checkbox" value="" disabled>
										  </label>
										</div>';
							 }
						  ?></td>
					</tr>
					<tr>
						<td>Norme 4G</td>
						<td><?php 
							if (($phone->norme_4g)>0) {
							 	echo '<div class="form-check disabled">
										  <label class="form-check-label">
										    <input class="form-check-input" type="checkbox" value="" checked disabled>
										  </label>
										</div>';
							 } 
							 else{
							 	echo '<div class="form-check disabled">
										  <label class="form-check-label">
										    <input class="form-check-input" type="checkbox" value="" disabled>
										  </label>
										</div>';
							 }
						  ?></td>
					</tr>
				</tbody>
			</table>
			
		</div>		
	<?php }?>

	
</body>
</html>