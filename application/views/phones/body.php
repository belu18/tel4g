	<title>Nos téléphones</title>
</head>
<body>
	<div class="row">
		<h1 class="col-lg-4 offset-lg-1">Nos téléphones</h1>
	</div>
	<div class="card-deck" id="telephones">
	<?php
		foreach ($phone_info as $phone) {?><!--  on récupère les données de la bdd via le select du model et on utilise un foeach pour les parcourir -->

			<div class=" col-sm-2 card card-inverse card-info mb-3 text-center" id="<?= $phone->id_tel ?>">
  				<img class="card-img-top" src="<?= base_url('assets/images/telephones/'.$phone->image)?>" alt="<?= $phone->image?>" style=" height:200px;">
 				 	<div class="card-block">
	 				 	<h2 class="card-title"><?= $phone->prix ?> €</h2>
	 				 	<h5 class="card-title"><?= $phone->marque ?></h5>
	 				 	<h5 class="card-title"><?= $phone->modele ?></h5>
  					</div>
			</div>			
	<?php }?>

	</div>
</body>
</html>