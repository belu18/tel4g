<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model
{
	protected $table='users';

	public function insert_users($nom,$prenom,$email,$datenaiss,$password)
	{// insertion des données dans la bdd
		$this->load->database();
		return $this->db->set('nom', $nom)
			->set('prenom',$prenom)
			->set('email', $email)
			->set('date_naissance', $datenaiss)
			->set('password', $password)
			->insert($this->table);
	}
	public function login_user($email,$password)
	{
		$this->load->database();
		return $this->db->select('*')
                        ->from($this->table)
                        ->where('email', $email)
                        ->where('password', $password)
                        ->get()
                        ->result();
	}
	public function erreur_email($email)
	{
		$this->load->database();
		return $this->db->select('email')
                        ->from($this->table)
                        ->where('email', $email)
                        ->get()
                        ->result();
	}
	public function erreur_password($email,$password)
	{
		$this->load->database();
		return $this->db->select('password')
                        ->from($this->table)
                        ->where('password', $password)
                        ->where('email', $email)
                        ->get()
                        ->result();
	}
}