<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Phones_model extends CI_Model
{
	public function get_info()
	{
		$this->load->database();
		//$resultat=array();
		$resultat = $this->db->select('*')
                     ->from('telephones')
                     ->get()
                     ->result();
         //var_dump($resultat);
         return $resultat;
		//	On simule l'envoi d'une requête
		/*return array('auteur' => 'Chuck Norris',
			     'date' => '24/07/05',
		             'email' => 'email@ndd.fr');*/
	}
	public function get_detail($id)
	{
		$this->load->database();
		//$resultat=array();
		$resultat = $this->db->select('*')
                     ->from('telephones')
                     ->where('id_tel',$id)
                     ->get()
                     ->result();
         //var_dump($resultat);
         return $resultat;
		//	On simule l'envoi d'une requête
		/*return array('auteur' => 'Chuck Norris',
			     'date' => '24/07/05',
		             'email' => 'email@ndd.fr');*/
	}
}