<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Abos_model extends CI_Model
{
	public function get_info_abos()
	{
		$this->load->database();
		$resultat = $this->db->select('*')
                     ->from('abonnements')
                     ->get()
                     ->result();
         return $resultat;
	}
	public function get_info_options()
	{
		$this->load->database();
		$resultat = $this->db->select('*')
                     ->from('options')
                     ->get()
                     ->result();
         return $resultat;
	}
}