#Dossier pour installer tel4g de Benoit Hasbroucq#

##Fichier sql##

allez dans [tel4g\data(tel4g\data) et vous trouverez le fichier sql pour la bdd



##Fichiers rajoutés pour URL rewriting##

-  rajout fichier tel4g.conf dans répertoire C:/wamp/alias
Ce fichier contient :

    `NameVirtualHost local.tel4g
    <VirtualHost local.tel4g>
    	DocumentRoot C:/wamp/www/integration/tel4g
	ServerName local.tel4g
    </VirtualHost>`

-  rajout fichier localhost.conf dans répertoire C:\wamp\alias

Ce fichier contient :

     NameVirtualHost localhost
     <VirtualHost localhost>
	DocumentRoot C:/wamp/www/
	ServerName localhost
     </VirtualHost>`
-rajout de la ligne dans fichier hosts dans répertoire C:\Windows\System32\drivers\etc\hosts

rajout de la ligne:

    127.0.0.1       local.tel4g

[Source](https://blog.smarchal.com/creer-un-virtualhost-avec-wampserver)